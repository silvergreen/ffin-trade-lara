<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('ticker');
            $table->string('name');
            $table->double('price', 8, 2);
            $table->double('current_price', 8, 2)->nullable();
            $table->double('price_LTP', 8, 2)->nullable();
            $table->double('prcie_LN', 8, 2)->nullable(); //price last nottification
            $table->integer('count')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
};
