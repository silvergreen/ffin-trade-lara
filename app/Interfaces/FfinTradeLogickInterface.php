<?php

namespace App\interfaces;

interface FfinTradeLogickInterface {
    public function getNotifyData ();
    public function sendNotification ();
    public function notificationFormat ($notifyData);
}

