<?php

namespace App\Library;
use App\interfaces\FfinTradeLogickInterface;
use App\Models\Ffin;
use App\Models\User;
use App\Providers\TelegramBotService;
use Illuminate\Support\Facades\Auth;

class FfinTradeLogickNotifycations implements FfinTradeLogickInterface
{   
    // procents changed for send notification
    private $stepSize = 5;
    public $tickersData;
    public $locatedTickersData;

    public function __construct($FfinData = null) {
        $this->tickersData = Ffin::convertToTickerObject($FfinData);
        $this->locatedTickersData = Ffin::ffinDbPurchases();
    }

    public function getNotifyData (){
        $notifyData = [];
        foreach ($this->locatedTickersData as $key => $value) {
            # code...
            $tickerC = $value->ticker;
            if(!empty($this->tickersData->$tickerC)){
                $notifyData[$value->user_id][] = $this->checkNotifyConditions($value,$this->tickersData->$tickerC);
            }
        }
        return $notifyData;
    }

    private function checkNotifyConditions ($dbTickerData,$todayTickerData){
        $reusult = new \stdClass();
        $todayPrice = $todayTickerData->bap>0?$todayTickerData->bap:$todayTickerData->ClosePrice;
        $todayLTP = $todayTickerData->ltp;
        $dbPrice = $dbTickerData->price;
        $dbLTP = $dbTickerData->price_LTP;
        $dbLNPrice = $dbTickerData->price_LN;
        $sendingStatus = false;


        $calcPricePercentage = ($todayPrice - $dbPrice) / ($dbPrice/100);
        //LNPrice - last notification price
        $calcLNPricePercentage = $todayPrice && $dbLNPrice ? ($todayPrice - $dbLNPrice) / ($dbLNPrice/100): 1;

        $calcPricePercentage = round($calcPricePercentage*100)/100;
        $calcLNPricePercentage = round($calcLNPricePercentage*100)/100;
        
        print_r('$calcLNPricePercentage');
        print_r($calcLNPricePercentage);
        print_r('<br>');
        print_r('<br>');
        print_r('<br>');

        if(!$dbLNPrice){
            $calcLNPricePercentage = $calcPricePercentage;
        }

        // condition currentPrice percentage bigger then last notification price on $stepSize
        if($calcLNPricePercentage < -$this->stepSize){
            Ffin::updateNotificationPrice($dbTickerData->id,$todayPrice);
            $sendingStatus = true;
        // condition currentPrice percentage smaller then last notification price on $stepSize
        }elseif ($calcLNPricePercentage > $this->stepSize) {
            Ffin::updateNotificationPrice($dbTickerData->id,$todayPrice);
            $sendingStatus = true;
        }elseif(!$dbLNPrice){
            Ffin::updateNotificationPrice($dbTickerData->id,$todayPrice);
        }

        $reusult->currentPercentage = $calcPricePercentage;
        $reusult->currentPrice = $todayPrice;
        $reusult->startPrice = $dbPrice;
        $reusult->ticker = $dbTickerData->ticker;
        $reusult->sendingStatus = $sendingStatus;
        
        return $reusult;
    }

    public function sendNotification (){
        $notifyData = $this->getNotifyData();
        foreach ($notifyData as $key => $value) {
            if(!empty($value)){
                $formatMessage = $this->notificationFormat($value);
                if($formatMessage->sendStatus){
                    $TelegramBotService = new TelegramBotService();
                    $user = User::where('id',$key)->firstOrFail();
                    $TelegramBotService->sendMessage($user->telegram_id,$formatMessage->formatedMessage);
                }
            }
        }
    }

    public function notificationFormat ($notifyData){
        $formatedMessage = '';
        $sendStatus = false;
        $notifyRes = new \stdClass();
        foreach ($notifyData as $key => $value) {
            var_dump($value);
            if($value->sendingStatus){
                $formatedMessage .= $value->ticker . ' changes = ' . $value->currentPercentage . '%  ' . "\r\n" . 'Initial price - ' .$value->startPrice. "\r\n" . 'current Price - ' . $value->currentPrice ."\r\n\r\n";
                $sendStatus = true;
            }
        }
        $notifyRes->formatedMessage = $formatedMessage;
        $notifyRes->sendStatus = $sendStatus;
        return $notifyRes;
    }
}
