<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Ffin;
use App\Nt\PublicApiClient;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreFfinTicker;
use Illuminate\Support\Facades\Auth;
use stdClass;
use App\Library\FfinTradeLogickNotifycations as Notify;

class FfinController extends Controller
{

    public function __constructor(StoreFfinTicker $storeInfoRequest)
   {}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ffinDbPurchases = Ffin::ffinDbPurchases();

        return view('app\ffinReport',["posts"=>'asdas',"results"=>$ffinDbPurchases]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app\ffinAddTicker');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFfinTicker $request)
    {

        // $request->validate()
        
        // print_r($request->ticker);

        $Ffin = new Ffin();
        $dataResult = $Ffin->ffinGetStockQuotes($request->ticker);
        if($dataResult){
            $tickerData = $Ffin->createStockQutes($dataResult,$request->count);
        }else{
            $tickerData = ['status'=>'error', 'message'=>"Something wrong"];
        }

        // print_r($dataResult);
        // print_r($tickerData);

        return redirect()->back()->with('query_data',['tickerData'=>$tickerData,'ticker'=>$request->ticker]);
    }

    public function cronUpdate()
    {

        $results = DB::select('select DISTINCT ticker from purchases ORDER BY ticker');

        foreach($results as $key => $value){
            $tickers[] = $value->ticker;
        }
        
        print_r($results);
        print_r($tickers);

        $Ffin = new Ffin();
        $dataResult = $Ffin->ffinGetMultipleStockQuotes($tickers);

        $responceDecode = json_decode($dataResult);
        $decodeDataResult = $responceDecode->result->q;

        $tickerData = $Ffin->updateStockQutes($decodeDataResult);

        $Notify = new Notify($decodeDataResult);
        $Notify->sendNotification();

        // echo '<script type="text/javascript">window.close();</script>';

        // 
        // 
        // if($dataResult){
        //     $tickerData = $Ffin->updateStockQutes($dataResult);
        // }else{
        //     $tickerData = ['status'=>'error', 'message'=>"Something wrong"];
        // }

        // return redirect()->back()->with('query_data',['tickerData'=>$tickerData,'ticker'=>$request->ticker]);

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
