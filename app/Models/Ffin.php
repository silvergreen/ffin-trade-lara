<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Nt\PublicApiClient;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class Ffin extends Model
{
    use HasFactory;
    private $apiKey = "1a24cfe2edabcc3f2f918540ef429b94";
    private $apiSecretKey = "17231dbd94060ac7c66d16579247d0138dc05d5b";

    public function ffinGetStockQuotes ($ticker) {
        /**
        
        * @param {string} $command - команда API
        * @param {array} $params - параметры команды
        * @param {'json'|'array'} $type - формат ответа json или array

        */

        // $result = $publicApiClient->sendRequest($command, $params, $type);

        
        $version = PublicApiClient::V2;

        $publicApiClient = new PublicApiClient($this->apiKey, $this->apiSecretKey, $version);

        $response = $publicApiClient->sendRequest('getStockQuotesJson',  ['tickers' => $ticker]);

        if(empty($response->error)){
            $responceDecode = json_decode($response);
            $dataResult = $responceDecode->result->q[0];
            return $dataResult;
        }

        return null;
    }

    public function ffinGetMultipleStockQuotes (array $tickers) {

        $version = PublicApiClient::V2;

        $publicApiClient = new PublicApiClient($this->apiKey, $this->apiSecretKey, $version);

        $response = $publicApiClient->sendRequest('getStockQuotesJson',  ['tickers' => $tickers]);

        return $response;
    }

    public static function ffinDbPurchases (){
        $id = Auth::id();
        
        $results = DB::select('select * from purchases');

        return $results;
    }

    public function createStockQutes (object $dataResult,int $count){
        $id = Auth::id();
        
        $price = $dataResult->bap>0?$dataResult->bap:$dataResult->ClosePrice;
        
        $insrtData = [$id,$dataResult->c,$dataResult->name,$price,$price, $dataResult->ltp, $count];
        //LTP - lasst traded price
        $insertResult = DB::insert('insert into purchases (user_id, ticker, name, price, current_price, price_LTP, count) values (?, ?, ?, ?, ?, ?, ?)', $insrtData);
        $answerData['insertResult'] = $insertResult;
    
        $answerData = ['status'=>'success', 'message'=>"Ticker added", 'data'=>$dataResult];

        return $answerData; 
    }

    public static function convertToTickerObject($tickersData){
        $i = 0;
        $sortedData = new \stdClass();

        while($i < count($tickersData))
        {   
            $tickerName = $tickersData[$i]->c;
            $sortedData->$tickerName =  $tickersData[$i];
            $i++;
        }

        return $sortedData;
    }

    public function updateStockQutes (array $dataResult){

        foreach ($dataResult as $key => $value) {
            // print_r($value->c);
            // print_r($value->bap);
            $updaateResult = DB::table('purchases')
            ->where('ticker', $value->c)
            ->update(['current_price' => $value->bap]);
        }
        
    
        $answerData = ['status'=>'success', 'message'=>"Ticker updated"];

        return $answerData; 
    }

    public static function updateNotificationPrice($id,$price){
        print_r('$id,$price');
        print_r($id,$price);
        print_r('$id,$price <br>');
        $updaateResult = DB::table('purchases')
        ->where('id', $id)
        ->update(['price_LN' => $price]);

        $answerData = ['status'=>'success', 'message'=>"Ticker updated"];
    }
}
