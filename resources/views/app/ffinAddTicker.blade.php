<x-layout bodyClass="g-sidenav-show  bg-gray-200">
<x-navbars.sidebar activePage="tables"></x-navbars.sidebar>
<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
<!-- Navbar -->
<x-navbars.navs.auth titlePage="Tables"></x-navbars.navs.auth>
<!-- End Navbar -->
<?php 
$query_data = [];
if(!empty(session('query_data'))){
    $query_data = session('query_data');
    $status = $query_data['tickerData']['status'];
    $message = $query_data['tickerData']['message'];
}

?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="row justify-content-center">
            <div class="col-6">
                <div class="card my-4">
                    <div class="card card-body">
                        <div class="card card-plain h-100">
                            <div class="card-header pb-0 p-3">
                                <div class="row">
                                    <div class="col-md-8 d-flex align-items-center">
                                        <h6 class="mb-3">Add New Ticker</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-3">
                                <form method="POST" action="{{route('StoreTicker')}}">
                                    @csrf <!-- {{ csrf_field() }} -->
                                    <div class="row">
                                        <div class="mb-3 col-md-12">
                                            <label class="form-label">Ticker</label>
                                            <input type="text" name="ticker" class="form-control border border-2 p-2"
                                                value="" placeholder="META.US" onfocus="focused(this)"
                                                onfocusout="defocused(this)">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="mb-3 col-md-12">
                                            <label class="form-label">Count Purchase</label>
                                            <input type="number" name="count" class="form-control border border-2 p-2"
                                                value="" placeholder="12" onfocus="focused(this)"
                                                onfocusout="defocused(this)">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn bg-gradient-dark">Submit</button>
                                </form>
                            </div>
                            <?php
                            
                            ?>
                            @if(!empty($status) && $status == 'success')   
                            
                            <div class="alert alert-success alert-dismissible text-white" role="alert">
                                <span class="text-sm">{{ $message }}</span>
                                <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @elseif(!empty($status) && $status == 'error')
                            <div class="alert alert-danger alert-dismissible text-white" role="alert">
                                <span class="text-sm">{{ $message }}</span>
                                <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-footers.auth></x-footers.auth>
</div>
</main>
<x-plugins></x-plugins>

</x-layout>